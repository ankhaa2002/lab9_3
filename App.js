import React from "react";
import {
  Input,
  KeyboardAvoidingView,
  Text,
  Button,
  VStack,
  Heading,
  Center,
  NativeBaseProvider,
} from "native-base";
import "react-native";
const Example = () => {
  return (
    <KeyboardAvoidingView
      h={{
        base: "400px",
      }}
    >
      <Center>
        <VStack flex="1" w="100%" maxW="300">
          <Heading mb="3">Нууц үг сэргээх</Heading>
          <Text color="muted.400">
            Та бүртгэлтэй и-мэйл хаягаа оруулна уу! Бид таны хаяг руу нэг
            удаагийн нууц үгийг илгээх болно.
          </Text>
          <Input placeholder="Email Address" mt="10" mb="4" />
          <Button mb="4">Илгээх</Button>
        </VStack>
      </Center>
    </KeyboardAvoidingView>
  );
};

export default () => {
  return (
    <NativeBaseProvider>
      <Center flex={1} px="3">
        <Example />
      </Center>
    </NativeBaseProvider>
  );
};


export default App;